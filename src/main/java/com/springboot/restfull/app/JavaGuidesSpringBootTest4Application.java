package com.springboot.restfull.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JavaGuidesSpringBootTest4Application {

	public static void main(String[] args) {
		SpringApplication.run(JavaGuidesSpringBootTest4Application.class, args);
	}

}
