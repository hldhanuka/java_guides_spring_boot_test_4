package com.springboot.restfull.app.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.springboot.restfull.app.model.Employee;

public interface EmployeeRepository extends JpaRepository<Employee, Long>{

}
